# Analog input reader from **Measurement Computing**'s USB-1608G device
======================================================================

http://www.mccdaq.com/usb-data-acquisition/USB-1608G-Series.aspx

## Using C-library

- Install libraries
    
    Follow the link below:
        
    http://www.mccdaq.com/TechTips/TechTip-9.aspx

- Compile source code
        
        gcc -g -Wall -I. -o signal_reader signal_reader.c ini/ini.c -L. -lmccusb  -lm -L/usr/local/lib -lhidapi-libusb -lusb-1.0 -lpthread

- Open `config.ini` and change settings if you wish.

- Execute binary
    
        sudo ./signal_reader

- Execute a python script to upload data to the server.
        
        python uploader.py


## Using python wrapper (Not tested)

Install library

    sudo pip install pydaqflex

Execute script

    sudo python signal_reader.py

