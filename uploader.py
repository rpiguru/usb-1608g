import ConfigParser
import base64
import glob
import logging.config
import os
import time
import zipfile
from os.path import basename

import requests

config = ConfigParser.RawConfigParser()
config.read('config.ini')
cur_dir = os.path.dirname(os.path.realpath(__file__)) + '/'

logging.config.fileConfig(cur_dir + "logging_usb1608g.ini")
logger = logging.getLogger("usb1608g")


def get_config(section, option, default_val=None):
    """
    Get configuration from `config.ini`
    :param default_val: Default value when current option not found
    :param section: Section name
    :param option: Option name
    :return:
    """
    try:
        val = config.get(section=section, option=option)
        return val
    except (ConfigParser.NoSectionError, ConfigParser.NoOptionError):
        return default_val


def upload_to_server(_filename):
    ingress_url = get_config('server', 'url')
    try:
        with open(_filename) as in_file:
            f_content = in_file.read()
        files = {'data': ('data', f_content)}
        r = requests.post(ingress_url, files=files)
        if r.status_code == requests.codes.ok:
            logger.debug('Uploaded {} to the server'.format(_filename))
            return True
        else:
            logger.warning('Failed to upload, status: {}'.format(r.status_code))
            r.raise_for_status()
    except Exception as e:
        logger.exception(e)


if __name__ == '__main__':

    logger.debug('Starting uploader script...')

    while True:
        dump_path = get_config('usb-1608g', 'save_path')

        time_stamp = str(int(time.time()))
        file_name = dump_path + '/' + time_stamp
        if len(glob.glob(dump_path + '/*.txt')) > 0:
            # Compress all txt files to a zip file
            zf = zipfile.ZipFile('{}.zip'.format(file_name), mode='w', compression=zipfile.ZIP_DEFLATED)
            try:
                for txt_file in sorted(glob.glob(dump_path + '/*.txt')):
                    zf.write(txt_file, basename(txt_file))
                    os.remove(txt_file)
            finally:
                zf.close()

            # base64 encode the content of zip file
            with open('{}.zip'.format(file_name), 'rb') as fin, open('{}.zip.b64'.format(file_name), 'w') as fout:
                base64.encode(fin, fout)

            # Now upload to server
            if upload_to_server('{}.zip.b64'.format(file_name)):
                os.remove('{}.zip'.format(file_name))
                os.remove('{}.zip.b64'.format(file_name))

        time.sleep(5)
