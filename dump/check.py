"""
    Check uploaded data
"""
import base64
import zipfile

file_name = 'test'      # Change file name

with open('{}.gz'.format(file_name), 'r') as fin, open('{}.zip'.format(file_name), 'wb') as fout:
    base64.decode(fin, fout)

with zipfile.ZipFile('{}.zip'.format(file_name), "r") as z:
    z.extractall()
