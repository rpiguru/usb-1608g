#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <ctype.h>
#include <math.h>
#include <unistd.h>
#include "ini/ini.h"
#include "pmd.h"
#include "usb-1608G.h"
#include <time.h>
#include <pthread.h>


#define FALSE 0
#define TRUE 1
#define MAX_COUNT 10485760     // Max size of Queue - 10M

pthread_mutex_t mutex[8];

float queue[8][MAX_COUNT];

int q_front[8], q_rear[8], q_cnt[8];
float *volts_val;
FILE *filePtr[8];
float table_AIN[NGAINS_1608G][2];
ScanList list[NCHAN_1608G];  // scan list used to configure the A/D channels.

typedef struct
{
    const char* mode;
    int channel_count;
    int gain[8];
    float frequency;
    int buf_len;
    const char* save_path;
    int save_interval;

} configuration;

configuration config;

static int ini_parser(void* user, const char* section, const char* name, const char* value)
{
    configuration* pconfig = (configuration*)user;

    #define MATCH(s, n) strcmp(section, s) == 0 && strcmp(name, n) == 0
    if (MATCH("usb-1608g", "frequency")){
        pconfig->frequency = atof(value);
    } else if (MATCH("usb-1608g", "buf_len")){
        pconfig->buf_len = atoi(value);
    } else if (MATCH("usb-1608g", "save_path")){
        pconfig->save_path = strdup(value);
    } else if (MATCH("usb-1608g", "save_interval")){
        pconfig->save_interval = atoi(value);
    } else if (MATCH("usb-1608g", "channel_count")) {
        pconfig->channel_count = atoi(value);
    } else if (MATCH("usb-1608g", "mode")) {
        pconfig->mode = strdup(value);
    } else if (MATCH("channel_gain", "ch0")) {
        pconfig->gain[0] = atoi(value);
    } else if (MATCH("channel_gain", "ch1")) {
        pconfig->gain[1] = atoi(value);
    } else if (MATCH("channel_gain", "ch2")) {
        pconfig->gain[2] = atoi(value);
    } else if (MATCH("channel_gain", "ch3")) {
        pconfig->gain[3] = atoi(value);
    } else if (MATCH("channel_gain", "ch4")) {
        pconfig->gain[4] = atoi(value);
    } else if (MATCH("channel_gain", "ch5")) {
        pconfig->gain[5] = atoi(value);
    } else if (MATCH("channel_gain", "ch6")) {
        pconfig->gain[6] = atoi(value);
    } else if (MATCH("channel_gain", "ch7")) {
        pconfig->gain[7] = atoi(value);
    } else {
        return 0;  /* unknown section/name, error */
    }
    return 1;
}

void init_mutex(void){
    int i;
    for (i = 0; i < 8; i++){
        mutex[i] = (pthread_mutex_t)PTHREAD_MUTEX_INITIALIZER;
    }
}

void init_queue(void){
    int i;
    for (i = 0; i < 8; i++){
        q_front[i] = 0;
        q_rear[i] = 0;
        q_cnt[i] = 0;
    }
}

void clear_queue(void){
    int i;
    for (i = 0; i < 8; i++){
        q_front[i] = q_rear[i];
        q_cnt[i] = 0;
    }
}

float put_queue(int ch, float val){
    if ((q_rear[ch] + 1) % MAX_COUNT == q_front[ch]){
        printf("Queue %d Overflow", ch);
        return(-1);
    }
    queue[ch][q_rear[ch]] = val;
    q_rear[ch] = (q_rear[ch] + 1) % MAX_COUNT;
    q_cnt[ch] ++;
    return val;
}

float get_queue(int ch){
    float val;
    if (q_front[ch] == q_rear[ch]){
        printf("Queue %d Underflow", ch);
        return -1;
    }
    val = queue[ch][q_front[ch]];
    q_front[ch] = (q_front[ch] + 1) % MAX_COUNT;
    q_cnt[ch] --;
//    printf("getting from queue: %5.3g", val);
    return val;
}


void *read_data(void *param){
/*
    Read data from USB-1608G and push to Queue
*/
    libusb_device_handle *udev = NULL;
    struct tm calDate;

    float temperature;

    int i, k;
    char serial[9];
    uint16_t version;
    uint16_t status;
    uint16_t data;
    uint16_t *sdataIn;          // holds 16 bit unsigned analog input data
    float *volts;               // holds analog voltage values
    int ret;

    uint8_t mode, gain, ch;

    udev = NULL;

    ret = libusb_init(NULL);
    if (ret < 0) {
        perror("libusb_init: Failed to initialize libusb");
        exit(1);
    }

    // There are 2 firmware versions of the board.  See MCC for details.
    if ((udev = usb_device_find_USB_MCC(USB1608G_V2_PID, NULL))) {
        printf("Success, found a USB 1608G!\n");
        usbInit_1608G(udev, 2);
    } else if ((udev = usb_device_find_USB_MCC(USB1608GX_V2_PID, NULL))) {
        printf("Success, found a USB 1608GX!\n");
        usbInit_1608G(udev, 2);
    } else if ((udev = usb_device_find_USB_MCC(USB1608GX_2AO_V2_PID, NULL))) {
        printf("Success, found a USB 1608GX_2AO!\n");
        usbInit_1608G(udev, 2);
    } else if ((udev = usb_device_find_USB_MCC(USB1608G_PID, NULL))) {
        printf("Success, found a USB 1608G!\n");
        usbInit_1608G(udev, 1);
    } else if ((udev = usb_device_find_USB_MCC(USB1608GX_PID, NULL))) {
        printf("Success, found a USB 1608GX!\n");
        usbInit_1608G(udev, 1);
    } else if ((udev = usb_device_find_USB_MCC(USB1608GX_2AO_PID, NULL))) {
        printf("Success, found a USB 1608GX_2AO!\n");
        usbInit_1608G(udev, 1);
    } else {
        printf("Failure, did not find a USB 1608G series device!\n");
        return 0;
    }

    //print out the wMaxPacketSize.  Should be 512
    printf("wMaxPacketSize = %d\n", usb_get_max_packet_size(udev,0));

    usbBuildGainTable_USB1608G(udev, table_AIN);
    for (i = 0; i < NGAINS_1608G; i++) {
        printf("Gain: %d   Slope = %f   Offset = %f\n", i, table_AIN[i][0], table_AIN[i][1]);
    }

    usbCalDate_USB1608G(udev, &calDate);
    printf("\n");
    printf("MFG Calibration date = %s\n", asctime(&calDate));

    usbGetSerialNumber_USB1608G(udev, serial);
    printf("Serial number = %s\n", serial);

    status = usbStatus_USB1608G(udev);
    printf("Status = %#x\n", status);

    usbTemperature_USB1608G(udev, &temperature);
    printf("Temperature = %.2f deg C  or  %.2f deg F \n", temperature, 9.0/5.0*temperature + 32.);

    version = 0xbeef;
    usbFPGAVersion_USB1608G(udev, &version);
    printf("FPGA version %02x.%02x\n", version >> 0x8, version & 0xff);

    // Stop current scan and clear FIFO
    usbAInScanStop_USB1608G(udev);
    usbAInScanClearFIFO_USB1608G(udev);

    if (strcmp(config.mode, "differential") == 0)
        mode = DIFFERENTIAL;
    else
        mode = SINGLE_ENDED;

    // Compose configuration parameters for AI
    for (ch = 0; ch < config.channel_count; ch++){
        switch(config.gain[ch]) {
            case 10: gain = BP_10V; break;
            case 5: gain = BP_5V; break;
            case 2: gain = BP_2V; break;
            case 1: gain = BP_1V; break;
            default:  gain = BP_10V; break;
        }
        list[ch].range = gain;
        list[ch].mode = mode;
        list[ch].channel = ch;
    }

    list[config.channel_count-1].mode |= LAST_CHANNEL;
    usbAInConfig_USB1608G(udev, list);

    if ((sdataIn = malloc(2*config.buf_len * config.channel_count)) == NULL) {
      perror("Can not allocate memory for sdataIn");
      return;
    }

    if ((volts = malloc(sizeof(float) * config.buf_len * config.channel_count)) == NULL) {
      perror("Can not allocate memory for voltage values");
      return;
    }

    // TODO: exact config option? see line 313 of `usb-1608G.c`
    usbAInScanStart_USB1608G(udev, 0, 0, config.frequency, 0x00);

    int cnt = 0;
    while(1){
        ret = usbAInScanRead_USB1608G(udev, config.buf_len, config.channel_count, sdataIn, 20000);
        if (ret == 0){
            printf("count: %d\n", cnt);
            usbAInScanStart_USB1608G(udev, 0, 0, config.frequency, 0x00);
            continue;
        }
        pthread_mutex_lock(&mutex[0]);
        for (i=0; i < config.channel_count * config.buf_len; i++){
            ch = i % config.channel_count;
            gain = list[ch].range;
            data = rint(sdataIn[i]*table_AIN[gain][0] + table_AIN[gain][1]);
            volts[i] = volts_USB1608G(gain, data);
            put_queue(0, volts[i]);
        }
        pthread_mutex_unlock(&mutex[0]);
//        for (ch = 0; ch < config.channel_count; ch++) {
//            for (i = 0; i < config.buf_len; i++) {
//                gain = list[ch].range;
//                k = i*config.channel_count + ch;
//                data = rint(sdataIn[k]*table_AIN[gain][0] + table_AIN[gain][1]);
//                volts[i] = volts_USB1608G(gain, data);
//            }
////            printf("---- Channel %d\n", ch);
//            pthread_mutex_lock(&mutex[0]);
//            for (i = 0; i < config.buf_len; i++) {
////                printf("%.2g ", volts[i]);
//                put_queue(ch, volts[i]);
//            }
//            pthread_mutex_unlock(&mutex[0]);
////            printf("\n");
//        }
//        break;
        cnt ++;
//        Sleep 3 ms
        usleep(3000);
    }
}

void *save_to_file(void *param){
/*
    Get data from Queue and save to file
    param: contains channel number
*/
    int ch = 0;
    int cnt = 0;
    int i, k;
    int buf_len = config.buf_len;
    int same_cnt = 0;

//    printf("Starting thread to save data of channel %d to file...\n", ch);
    char tmp_file_name[30][30];
    char txt_file_name[30][30];
    for (i=0; i < config.channel_count; i++){
        sprintf(tmp_file_name[i], "%s/ch%d_%lu.tmp", config.save_path, i, (unsigned long)time(NULL));
        sprintf(txt_file_name[i], "%s/ch%d_%lu.txt", config.save_path, i, (unsigned long)time(NULL));

    }

    if ((volts_val = malloc(sizeof(float)*(config.buf_len) * config.channel_count)) == NULL) {
      perror("Can not allocate memory for voltage values");
      return;
    }

    while(1){
        pthread_mutex_lock(&mutex[0]);
        if (q_cnt[0] < buf_len * config.channel_count){
            pthread_mutex_unlock(&mutex[0]);
            usleep(1000);
        }
        else{
            for (i = 0; i < buf_len * config.channel_count; i++) {
                volts_val[i] = get_queue(0);
            }
            pthread_mutex_unlock(&mutex[0]);
            for (ch = 0; ch < config.channel_count; ch++){
                if (cnt == 0)
                    filePtr[ch] = fopen(tmp_file_name[ch], "w");
                else
                    filePtr[ch] = fopen(tmp_file_name[ch], "a");

                for (i = 0; i < config.buf_len; i++) {
                    k = config.channel_count * i + ch;
                    fprintf(filePtr[ch], "%.4g ", volts_val[k]);
                }
                fclose(filePtr[ch]);
            }

            cnt += buf_len;
            if (cnt >= config.save_interval * config.frequency){
                printf("Data received, saving to file: %s, length: %d\n", txt_file_name, cnt);
//              Assign new file name
                for (ch = 0; ch < config.channel_count; ch++){
                    rename(tmp_file_name[ch], txt_file_name[ch]);
                    sprintf(tmp_file_name[ch], "%s/ch%d_%lu.tmp", config.save_path, ch, (unsigned long)time(NULL));
                    sprintf(txt_file_name[ch], "%s/ch%d_%lu.txt", config.save_path, ch, (unsigned long)time(NULL));
                }
                cnt = 0;
            }
        }
    }
}


int main (int argc, char **argv)
{
    init_mutex();

    init_queue();

    if (ini_parse("config.ini", ini_parser, &config) < 0) {
        printf("Can't load 'config.ini'\n");
        return 1;
    }
    printf("\nConfig loaded from 'config.ini':\n mode=%s\n channels=%d\n gain=%d\n frequency=%f\n buf_len=%d\n save_path=%s\n",
        config.mode, config.channel_count, config.gain, config.frequency, config.buf_len, config.save_path);

    pthread_t t_read;
    pthread_create(&t_read, NULL, read_data, NULL);

    pthread_t t_save[8];
    int i;
//    for (i=0; i < config.channel_count; i++){
    for (i=0; i < 1; i++){
        int *arg = malloc(sizeof(*arg));
        if (arg == NULL){
            fprintf(stderr, "Couldn't allocate memory for thread arg.\n");
            return 1;
        }
        *arg = i;
        pthread_create(&t_save[i], 0, save_to_file, arg);
    }

    pthread_join(t_read, NULL);
    for (i=0; i < 8; i++){
        pthread_join(t_save[i], NULL);
    }

}
