from daqflex import MCCDevice


class USB1608G(MCCDevice):
    """USB-1608G card"""
    fpga_image = 'firmware/USB_1608G.rbf'
    max_counts = 0xFFFF
    id_product = 0x0134

    slope = [0] * 8
    offset = [0] * 8

    def __init__(self, **kwargs):
        super(USB1608G, self).__init__(**kwargs)
        # Initialize calibration parameters
        for i in range(8):
            self.slope[i], self.offset[i] = self.get_calib_data(i)


if __name__ == '__main__':

    inst = USB1608G()
    for ch in range(8):
        print inst.get_calib_data(ch)

    inst.start_continuous_transfer(rate=50000, buf_size=None)

    while True:
        data = inst.read_scan_data(length=100, rate=50000)
        print data.tolist()
